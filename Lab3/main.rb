require '../Lib/user_option_parser.rb'
require '../Lib/constants.rb'
require '../Lib/chats/broadcast_chat.rb'
require '../Lib/chats/multicast_chat.rb'

parser = UserOptionParser.new
options = parser.parse

if(Constants::CAST_TYPES.include?(options.cast))
  chat = nil
  case options.cast
  when 'broadcast'
    chat = Chats::BroadcastChat.new(options.name)
  when 'multicast'
    chat = Chats::MulticastChat.new(options.name)
  end
  chat.start
else
  abort("Please, choose cast type: 'broadcast' or 'multicast'")
end