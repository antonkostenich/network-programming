require 'optparse'
require 'ostruct'

class UserOptionParser
  def initialize
    @options = OpenStruct.new
  end
  def parse
    @opt_parser = OptionParser.new do |opt|
      opt.banner = "Usage:  #{File.basename($PROGRAM_NAME)} [OPTIONS]"
      opt.separator ""
      opt.separator "SPECIFIC OPTIONS:"

      opt.on("-c CAST", "CastType, i.e. 'broadcast' or 'multicast'") do |n|
        @options.cast = n
      end

      opt.on("-n NAME", "Name in chat") do |n|
        @options.name = n
      end

      opt.separator ""
      opt.separator "COMMON OPTIONS:"

      opt.on_tail("-h", "Show help message") do
        puts opt
        exit
      end

      opt.on_tail("-v", "Show version") do
        puts "Option parser by eXiga, version 1.0"
        exit
      end
    end.parse!
    return @options
  end
end