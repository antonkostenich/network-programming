require 'socket'

module Helpers
  def Helpers.calculate_checksum(msg)
    length = msg.length
    num_short = length / 2
    check = 0

    msg.unpack("n#{num_short}").each do |short|
      check += short
    end

    if length % 2 > 0
      check += msg[length - 1, 1].unpack('C').first << 8
    end

    check = (check >> 16) + (check & 0xffff)
    return (~((check >> 16) + check) & 0xffff)
  end

   def Helpers.generate_name
      name = (0...8).map{(65+rand(26)).chr}.join
      name
   end

   def Helpers.get_ip_address
     ips = Socket::ip_address_list
     
     ips.each do |e|
       if(e.ip_address.split('.').first == '192')
         return e.ip_address
       else
         nil
       end
     end
   end
end