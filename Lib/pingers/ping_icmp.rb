require_relative 'ping.rb'
require_relative 'constants.rb'
require_relative 'helpers.rb'

class Ping::ICMP < Ping
  attr_reader :seq, :received_data_length

  def initialize(host = nil, port = 0)
    @seq = -1
    @data = ''
    @pid = Process.pid & 0xffff

    0.upto(Constants::DATA_SIZE) { |n| @data << (n % 256).chr }
    super(host, port, Constants::TIMEOUT)
  end

  def ping
    super(@host)
    result = true

    @seq = (@seq + 1) % 65536
    message = build_message

    socket = Socket.new(Socket::PF_INET, Socket::SOCK_RAW, Socket::IPPROTO_ICMP)
    begin
      saddr = Socket.sockaddr_in(0, host)
    rescue Exception
      socket.close unless socket.closed?
      return result
    end

    start_time = Time.now
    socket.send(message, 0, saddr)

    begin
      Timeout.timeout(@timeout) {
        while true
          io_array = select([socket], nil, nil, Constants::TIMEOUT)
          if io_array.nil? || io_array[0].empty?
            return false
          end

          data = socket.recvfrom(1500).first
          type = data[20, 2].unpack(Constants::ICMP_MSG_TEMPLATE[0, 2]).first
          @received_data_length = data.length

          case type
          when Constants::ICMP_ECHOREPLY
            if data.length >= 28
              pid, seq = data[24, 4].unpack(Constants::ICMP_MSG_TEMPLATE[3, 2])
            end
          else
            if data.length > 56
              pid, seq = data[52, 4].unpack(Constants::ICMP_MSG_TEMPLATE[3, 2])
            end
          end

          if pid == @pid && seq == @seq && type == Constants::ICMP_ECHOREPLY
            result = true
            break
          end
        end
      }
    rescue Exception => e
      p e
    ensure
      socket.close if socket
    end

    @duration = Time.now - start_time if result

    return result
  end

  private

  def build_message
    template_string = Constants::ICMP_MSG_TEMPLATE + Constants::DATA_SIZE.to_s

    checksum = 0
    msg = [Constants::ICMP_ECHO, Constants::ICMP_SUBCODE, checksum, @pid, @seq, @data].pack(template_string)

    checksum = Helpers::calculate_checksum(msg)
    msg = [Constants::ICMP_ECHO, Constants::ICMP_SUBCODE, checksum, @pid, @seq, @data].pack(template_string)

    msg
  end
end