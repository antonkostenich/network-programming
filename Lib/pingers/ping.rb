require 'socket'
require 'timeout'

class Ping
  attr_accessor :host, :port, :timeout
  attr_reader :duration

  def initialize(host = nil, port = 0, timeout = 5)
    @host = host
    @port = port || Socket.getservbyname('echo')
    @timeout = timeout
    @duration = nil
  end

  def ping(host = @host)
    raise ArgumentError, 'No host' unless host
    @duration = nil
  end
end