module Constants
  ICMP_ECHOREPLY = 0
  ICMP_ECHO = 8
  ICMP_SUBCODE = 0
  ICMP_MSG_TEMPLATE = 'C2 n3 A'

  DATA_SIZE = 64
  TIMEOUT = 5

  CAST_TYPES = ['broadcast', 'multicast']

  BROADCAST_HOST = '<broadcast>'
  BROADCAST_PORT = 33333 # idk why we should use this port, but it's working

  MULTICAST_HOST = '224.0.0.1'
  MULTICAST_PORT = 3000 # the same thing as in broadcast, i have no idea about this magic numbers 

  GET_IP = 'get_ip'
  GET_LIST = 'get_list'
  IP_MSG = 'IP:'
  IP_REMOVE_MSG = 'DIP:'
end