require 'socket'
require_relative 'chat.rb'
require_relative '../constants.rb'

module Chats
  class BroadcastChat < Chat

    def initialize(name)
      @listening_socket = UDPSocket.new
      @listening_socket.bind(Constants::BROADCAST_HOST, Constants::BROADCAST_PORT)

      @sending_socket = UDPSocket.new
      @sending_socket.setsockopt(Socket::SOL_SOCKET, Socket::SO_BROADCAST, true)

      super(name)
    end

    def send_data(msg = gets)
      @sending_socket.send("<#{@name}> #{msg}", 0, Constants::BROADCAST_HOST, Constants::BROADCAST_PORT)
    end
  end
end