require 'ipaddr'
require_relative 'chat.rb'
require_relative 'chat.rb'
require_relative '../constants.rb'

module Chats
  class MulticastChat < Chat

    def initialize(name)
      @listening_socket = UDPSocket.new
      membership = IPAddr.new(Constants::MULTICAST_HOST).hton + IPAddr.new(Helpers.get_ip_address).hton
      @listening_socket.setsockopt(Socket::IPPROTO_IP, Socket::IP_ADD_MEMBERSHIP, membership)
      @listening_socket.setsockopt(Socket::SOL_SOCKET, Socket::SO_REUSEPORT, 1)
      @listening_socket.bind(Constants::MULTICAST_HOST, Constants::MULTICAST_PORT)

      @sending_socket = UDPSocket.new
      @sending_socket.setsockopt(Socket::IPPROTO_IP, Socket::IP_MULTICAST_TTL, 1)

      super(name)
    end

    def send_data(msg = gets)
      sending_socket.send("<#{@name}> #{msg}", 0, Constants::MULTICAST_HOST, Constants::MULTICAST_PORT)
    end
  end
end