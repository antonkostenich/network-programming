require_relative '../helpers.rb'

module Chats
  class Chat
    attr_accessor :listening_socket, :sending_socket, :name
    attr_accessor :ip, :ip_list

    def initialize(name)
      @name = name.nil? ? Helpers.generate_name : name
      @threads = []
      @ip_list = []

      @ip = Helpers.get_ip_address
      @ip_list << @ip
    end

    def start
      self.initialize_threads
      begin
        @threads.each { |thread| thread.join }
      rescue Exception => e
        p e.to_s
      ensure
        stop
      end
    end

    def stop
      send_data(Constants::IP_REMOVE_MSG + ' ' + @ip)
      @threads.each { |thread| thread.exit }
      @listening_socket.close
      @sending_socket.close
    end

    def receive_data
      msg = @listening_socket.recv(1024)
      if !handle_ip_commands?(msg)
        puts msg
      end
    end

    def handle_ip_commands?(msg)
      handled = false
      case msg.split[1]
      when Constants::GET_IP
        send_data(@ip)
        handled = true
      when Constants::GET_LIST
        p @ip_list
      when Constants::IP_MSG
        @ip_list << msg.split[2]
        handled = true
      when Constants::IP_REMOVE_MSG
        if @ip_list.include?(msg.split[2])
          @ip_list.delete(msg.split[2])
        end
      end

      return handled
    end

    def send_data
      raise NotImplementedError, 'Override this method'
    end

     def initialize_threads
      listening_thread = Thread.new do
        loop do
          receive_data
        end
      end
      @threads << listening_thread
      sending_thread = Thread.new do
        loop do
          send_data
        end
      end
      @threads << sending_thread
    end
  end
end
