require_relative '../Lib/pingers/ping_icmp.rb'

host_to_ping = ARGV[0]

if !host_to_ping
  abort('Please pass a host you want to ping as a parameter')
end

pinger = Ping::ICMP.new(host_to_ping)
repeat = 5

puts "PING #{host_to_ping}: #{Constants::DATA_SIZE} data bytes"
(1..repeat).each do
  if pinger.ping
    puts "#{pinger.received_data_length} from #{host_to_ping}: icmp_seq=#{pinger.seq} time=#{pinger.duration} ms"
  else
    puts "Request timeout for icmp_seq #{pinger.seq}"
  end
end