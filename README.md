# Network Programming labs, BSUIR, 2014

Pull requests are welcome.

# Branch naming rules:

problem/feature_NP#{changed_module}_short_description

# Commit comments rules:

NP#{changed_module} - Short and clear description
